# Group: 8-bit Federation (Team 8)
# Muaath Alaraj
# Doan Dinh
# Date: 4/5/2018
# LAB 13 Problem 2
# VERSION 3.0

# Classes to make configuration of the map and items easier


class Location:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y


class Room:
    def __init__(self, name, message):
        self.name = name
        self.message = message
        self.items = []
        self.npcs = []

    def addItem(self, item):
        return self.addItems([item])

    def addItems(self, items):
        self.items.extend(items)
        return self

    def addNPC(self, npc):
        self.npcs.append(npc)
        return self


class Item:
    def __init__(self, name, description):
        self.name = name
        self.description = description


class Ghost:
    def __init__(self):
        self.name = 'Ghost'


class Inventory:
    def __init__(self):
        self.items = []

    def add(self, item):
        self.items.append(item)
        return self

    def contains(self, name):
        for item in self.items:
            if item.name == name:
                return True
        return False

    def remove(self, name):
        for item in self.items:
            if item.name == name:
                self.items.remove(item)
        return self

    def clear(self):
        self.items = []
        return self

    def __len__(self):
        return len(self.items)

    def __str__(self):
        return ', '.join(item.name for item in self.items)


# Items
keyUpper = Item(
    'keyupper', 'The upper part of a rusted key, the rest appears to be missing...')
keyLower = Item(
    'keylower', 'The lower piece of a rusted key, where could the rest be...')
keyComplete = Item('keycomplete', 'A rusted key...what does it open?')

# NPCs
ghost = Ghost()

# Rooms
basement = Room(
    'Basement', 'A dark basement, in the corner you see a strange illumination...').addItem(keyUpper)
bathroom = Room(
    'Bathroom', 'Here is a bathroom. The bathtub seems to be filled with a red liquid. Is that blood?')
library = Room('Library',
               'On one of the walls you see a portrait with an eerie photograph. Its eyes seem to follow you as you move around the room.')

bedroom = Room('Bedroom', 'Welcome to the bedroom. The bedsheets are really dirty.\n'
                          "You see a message written in blood on the mirror. Red trails drip from the letters, it says,\n\n"
                          "Someplace\n"
                          "Evil\n"
                          "Could\n"
                          "Rapidly\n"
                          "End\n"
                          "This\n")

kitchen = Room('Kitchen',
               'Flies are swarming around you. Could they be in here because of that strange meat on the cutting board?')
dinningRoom = Room('Dining Room',
                   'In the dining room the table is neatly set. But the cups seem to be filled with beet juice...no wait.').addItem(
    keyLower)

ballRoom = Room('Ballroom',
                'There is an awful smell here, hanging from the ceiling is a skeleton wearing a wedding dress.')
billardRoom = Room('Billiard Room',
                   'Snarling animal heads mounted on the high walls look down on you as you stumble upon an overturned billiard table.')
balcony = Room('Balcony', 'Welcome to the balcony. A wave of depression washes over you and you feel hopeless.').addNPC(
    ghost)

frontDoor = Room(
    'Front Door', 'You see the front door. Unfortunately it is locked.')

secretRoom = Room('Secret Room',
                  'You enter a secret room through a small trapdoor. The trapdoor shuts behind you. A literal TRAP DOOR. You might as well \'kill\' yourself. But well done finding the secret room.')

world = [
    [basement, bathroom, library],
    [bedroom, kitchen, dinningRoom],
    [ballRoom, billardRoom, balcony],
    [None, frontDoor, None],
    [None, None, None],
    [None, secretRoom, None],
    [None, None, None]
]

# Logic
location = Location()
inventory = Inventory()


def look():
    room = getCurrent()
    print room.message
    if len(room.items) > 0:
        print 'In the %s you see the items: %s' % (room.name, ', '.join(item.name for item in room.items))

    # gather surrounding data
    for d in ['north', 'south', 'east', 'west']:
        loc = canMove(d[:1])
        if loc is None:
            continue
        print 'To the %s you see a %s' % (d, getCurrent(loc).name)


def getCurrent(loc=None):
    loc = location if loc is None else loc
    return world[loc.y][loc.x]


def canMove(direction):
    # copy the current location
    newLoc = Location(location.x, location.y)

    # just do the move, check later
    if direction == 'n':
        newLoc.y -= 1
    elif direction == 's':
        newLoc.y += 1
    elif direction == 'w':
        newLoc.x -= 1
    else:
        newLoc.x += 1

    # if it's not in the bounds return false
    if newLoc.x < 0 or newLoc.x > len(world[0]) - 1 or newLoc.y < 0 or newLoc.y > len(world) - 1:
        return None

    # check that the map actually has a room to move to
    if not isinstance(getCurrent(newLoc), Room):
        return None

    # all checks passed return the new location
    return newLoc


def move(direction):
    # the function needs to be able to update the location
    global location
    # see if we can move in the direction requested
    loc = canMove(direction)
    if loc is None:
        print 'After seeing the edge of the earth and your life flashes before your eyes, you decided not to move...'
        return
    # commit the actual move
    location = loc


def take(name):
    global inventory

    if not name:
        print 'You must pick an item to take nothing selected'
        return

    room = getCurrent()
    for i, item in enumerate(room.items):
        if item.name == name:
            # add item to the inventory and remove from the room
            inventory.add(room.items.pop(i))

            print 'You snatched the %s and put it in your bag' % item.name
            return

    print 'There is no %s in the %s' % (name, room.name)


def bag():
    if len(inventory) > 0:
        print 'In your bag, you have: %s' % inventory
    else:
        print 'Your bag is empty'


def gameOver():
    print '\nGame Over!'
    print 'Play again if you dare...'


def isGameOver():
    # check if player ran into a ghost
    room = getCurrent()
    for npc in room.npcs:
        if isinstance(npc, Ghost):
            print 'You lose control of your body and slowly see the light fade away as an evil ghost inhabits your body\n' \
                  'You see flashbacks of all of the horrors that took place in this mansion and ponder the best way to\n' \
                  'torment the next visitor as a light approaches near the entryway'
            gameOver()
            return True
    return False


def combineKeys():
    global inventory

    if not (inventory.contains('keyupper') or inventory.contains('keylower')):
        print 'How am I supposed to make a key without the other half...I wonder where it could be?'
        return

    inventory.clear()
    inventory.add(keyComplete)
    print 'The two halves of the key click together and you feel hopeful...'


def openDoor():
    # does the user have the key? if they do they win
    if not inventory.contains('keycomplete'):
        print 'A key is required to open the door. Two halves are somewhere in the mansion.\n' \
              'Combine them once you have them to escape'
        return False
    print 'You live you see another day...for now! Be careful to not lock yourself someplace you cannot escape...'
    return True


def secret():
    global location
    location.x = 1
    location.y = 5


def help():
    welcome()
    print 'List of commands: '
    print '  s or south - to go south'
    print '  w or west - to go west'
    print '  n or north - to go north'
    print '  e or east - to go east'
    print '  look - to display a description of the room'
    print '  take - to take item in room'
    print '  combine - to combine the items'
    print '  kill - to end everything right then and there'
    print '  open - to attempt opening a door'
    print '  bag - to view your inventroy'
    print '  exit - to exit the game'


def welcome():
    print ''
    print 'Hello Welcome to the Mansion Escape'
    print 'Can you survive till the end?'
    print 'At any moment in the game type in \'help\' for the list of commands.'


welcome()
while not isGameOver():
    print ''
    look()
    name = requestString("Enter your name fellow adventurer")
    cmd = requestString("Type an action, or \'help\' ")
    if cmd is None:
        continue

    cmd = cmd.lower()

    if cmd == 'look':
        look()
    elif cmd in ['w', 'west', 'e', 'east', 's', 'south', 'n', 'north']:
        # only send the first letter
        move(cmd[:1])
    elif cmd.startswith('take'):
        # remove take if it's there and strip whitespace
        item = cmd.split('take', 1)[-1].strip()
        take(item)
    elif cmd == 'bag':
        bag()
    elif cmd == 'kill':
        print name + ' you have lost the will to continue on and moved into the netherworld...'
        gameOver()
        break
    elif cmd == 'combine':
        combineKeys()
    elif cmd == 'open':
        if openDoor():
            break
    elif cmd == 'secret':
        secret()
    elif cmd == 'help':
        help()
    elif cmd == 'exit':
        print name + ', thanks for playing!'
        break
    else:
        print name + ' you are lost dumby, confused, and do not know what to do...'
