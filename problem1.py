# Group: 8-bit Federation (Team 8)
# Muaath Alaraj
# Doan Dinh
# Date: 4/5/2018
# LAB 13 Problem 1
# VERSION 3.0

article = 'The statement quoted Yulia Skripal as saying her strength is growing daily.Ms Skripal, 33, and her father are in hospital in Salisbury, southern England, where the attack took place.\
The UK Foreign Office says Ms Skripal, who is a Russian citizen, has not yet taken up Russias offer of consular assistance.\
The Russian embassy in London said last week that it was insisting on its right to see her after it emerged that she was conscious and talking.\
Earlier on Thursday, Russian TV aired a recording of an alleged phone conversation between Ms Skripal and her cousin.\
However, doubts have been cast on how authentic the recording is; the presenters of the programme said they had been unable to verify it and themselves had doubts about it.\
Mr Skripal, 66, remains critically ill but stable.\
The UK government has accused Russia of being behind the 4 March attack, but Russias ambassador in the UK said Moscow had no nerve agent stockpile.\
The incident has sparked an international diplomatic crisis.\
The United Nations Security Council has been discussing the poisoning in a meeting called by Russia. \
Ambassador Vassily Nebenzia described the British accusations against Russia as horrific and unsubstantiated.'

madLib = ['Foriegn', 'Russia', 'Russian', 'Russias',
          'London', 'UK', 'Government', 'attack']
game = True
count = 0
newCount = 0
listWords = []
#articleWords = []

# for word in article:
#    articleWords.append(word)
#    print(articleWords)

while(game):
    word = raw_input('Enter word number  ' + str(count) + ':')
    # madLib.append(word)

    listWords.append(word)
    count = count + 1
    if count == len(madLib):
        game = False

# listWords.append(article.split()).append(madLib)
print(listWords)
arcleList = article.split()
mergedList = arcleList
for n in mergedList:
    for ml in range(len(madLib)):
        if n == madLib[ml]:
            mergedList[newCount] = listWords[ml]
    newCount = newCount + 1
print(" ".join(mergedList))
